FROM gcr.io/projectsigstore/cosign:latest as cosign-bin


FROM gitlab-registry.cern.ch/linuxsupport/c8-base:latest

MAINTAINER Bertrand NOEL <bertrand.noel@cern.ch>, Ricardo Rocha <ricardo.rocha@cern.ch>

# nice to have utilities
RUN yum install -y \
	ca-certificates \
	gcc \
	git \
	jq \
	make \
	man-pages \
	sudo \
	vim \
	wget \
	&& yum clean all

RUN curl https://download-ib01.fedoraproject.org/pub/epel/8/Everything/x86_64/Packages/s/s3cmd-2.3.0-1.el8.noarch.rpm -o s3cmd.noarch.rpm && \
	dnf install -y s3cmd.noarch.rpm && \
	rm -rf s3cmd.noarch.rpm

# CERN CA
ADD cerngridca.crt /etc/pki/ca-trust/source/anchors/cerngridca.crt
ADD cernroot.crt /etc/pki/ca-trust/source/anchors/cernroot.crt
RUN update-ca-trust

# krb and afs configuration
RUN echo $'\n\
[openafs-stable] \n\
name=openafs [stable] \n\
baseurl=https://linuxsoft.cern.ch/cern/centos/8/openafs/x86_64 \n\
enabled=1 \n\
gpgcheck=False \n\
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-koji file:///etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2 \n\
priority=1 \n\
protect=1 \n'\
>> /etc/yum.repos.d/openafs-stable.repo

RUN yum -y install \
	krb5-workstation \
	openafs-krb5 \
	&& yum clean all
ADD krb5.conf /etc/krb5.conf

# rpm/koji rpms and setup
RUN echo $'\n\
[linuxsupport8-stable] \n\
name=linuxsupport [stable] \n\
baseurl=http://linuxsoft.cern.ch/internal/repos/linuxsupport8-stable/x86_64/os \n\
enabled=1 \n\
gpgcheck=False \n\
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-koji file:///etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2 \n\
priority=1 \n\
protect=1 \n'\
>> /etc/yum.repos.d/linuxsupport-stable.repo

RUN yum -y install python3-libcomps && yum clean all
RUN curl https://download-ib01.fedoraproject.org/pub/epel/8/Everything/aarch64/Packages/p/python3-requests-gssapi-1.2.2-1.el8.noarch.rpm -o python3-requests-gssapi.noarch.rpm && \
	dnf install -y python3-requests-gssapi.noarch.rpm && \
	rm -rf python3-requests-gssapi.noarch.rpm
RUN yum install -y --disablerepo=extras \
	python3-koji \
	koji \
	&& yum clean all

RUN yum install -y \
	rpm-build \
	rpmdevtools \
	&& yum clean all

ADD koji.conf /etc/koji.conf
ADD afs/etc /usr/vice/etc

RUN rpmdev-setuptree

# openstack clients
RUN echo $'\n\
[cci7-openstack-clients-stable] \n\
name=CERN rebuilds for OpenStack clients - QA \n\
baseurl=http://linuxsoft.cern.ch/internal/repos/openstackclients-ussuri8s-stable/x86_64/os/ \n\
enabled=1 \n\
gpgcheck=0 \n\
priority=1 \n'\
>> /etc/yum.repos.d/openstackclients8-ussuri-stable.repo

RUN echo $'\n\
[centos8-cloud-openstack-victoria] \n\
name=Openstack RDO \n\
baseurl=https://linuxsoft.cern.ch/cern/centos/8/cloud/x86_64/openstack-victoria \n\
enabled=1 \n\
priority=1 \n\
gpgcheck=0 \n'\
>> /etc/yum.repos.d/centos8-cloud-openstack-victoria.repo

RUN yum install --disablerepo=extras --disablerepo=epel -y \
	python3-barbicanclient \
	python3-decorator \
	python3-heatclient \
	python3-ironic-inspector-client \
	python3-keystoneclient \
	python3-openstackclient \
	python3-swiftclient \
	python3-cryptography \
	python3-ironicclient \
	python3-magnumclient \
	python3-manilaclient \
	python3-mistralclient \
	&& yum clean all

RUN yum install -y \
    slirp4netns \
    && yum clean all

# docker client (upstream)
RUN echo $'\n\
[docker-ce-stable] \n\
name=Docker CE Stable - $basearch \n\
baseurl=https://download.docker.com/linux/centos/8/$basearch/stable \n\
enabled=1 \n\
gpgcheck=0 \n\
gpgkey=https://download.docker.com/linux/centos/gpg \n'\
>> /etc/yum.repos.d/docker.repo

# artifact utilities
COPY --from=cosign-bin /ko-app/cosign /usr/local/bin/cosign

RUN echo $'\n\
[authz8-stable] \n\
name=AuthZ Packages [stable] \n\
baseurl=http://linuxsoft.cern.ch/internal/repos/authz8-stable/x86_64/os \n\
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-koji file:///etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2 \n\
includepkgs=auth-get-sso-cookie \n\
enabled=1 \n\
priority=1 \n\
gpgcheck=0 \n'\
>> /etc/yum.repos.d/authz8-stable.repo

RUN yum install -y \
	auth-get-sso-cookie \
	docker-ce-20.10.12 \
	&& yum clean all

RUN curl -L https://github.com/docker/compose/releases/download/1.16.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose; \
	chmod +x /usr/local/bin/docker-compose

RUN curl -L https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl > /usr/local/bin/kubectl; \
	chmod +x /usr/local/bin/kubectl

RUN curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash

RUN LATEST=$(curl -L https://golang.org/VERSION?m=text) && cd /tmp && \
	curl -L https://dl.google.com/go/$LATEST.linux-amd64.tar.gz --output go.linux-amd64.tar.gz && \
	tar zxvf go.linux-amd64.tar.gz && \
	mv go /usr/local && \
	ln -s /usr/local/go/bin/go /usr/bin/go && \
	ln -s /usr/local/go/bin/gofmt /usr/bin/gofmt && \
	rm go.linux-amd64.tar.gz

RUN curl -sfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | \
	sh -s -- -b "/usr/local/go/bin" $(curl -s https://github.com/golangci/golangci-lint/releases/latest | grep -Eo 'tag/v[0-9.]+' | awk -F"/" '{print $2}') && \
	ln -s /usr/local/go/bin/golangci-lint /usr/bin/golangci-lint

ENV SHELL=bash

ADD entry.sh /entry.sh

CMD /entry.sh
